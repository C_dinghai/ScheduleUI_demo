const resolve = require('path').resolve
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const url = require('url')
const publicPath = ''

module.exports = (options = {}) => ({
  entry: {
    vendor: './src/vendor',
    index: './src/main.js'
  },
  output: {
    path: resolve(__dirname, 'dist'),
    filename: options.dev ? '[name].js' : '[name].js?[chunkhash]',
    chunkFilename: '[id].js?[chunkhash]',
    publicPath: options.dev ? '/assets/' : publicPath
  },
  module: {
    rules: [{
        test: /\.vue$/,
        use: ['vue-loader']
      },
      {
        test: /\.js$/,
        use: ['babel-loader'],
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader', 'postcss-loader']
      },
      {
        test: /\.(png|jpg|jpeg|gif|eot|ttf|woff|woff2|svg|svgz)(\?.+)?$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000
          }
        }]
      }
    ]
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      names: ['vendor', 'manifest']
    }),
    new HtmlWebpackPlugin({
      template: 'src/index.html'
    })
  ],
  resolve: {
    alias: {
      '~': resolve(__dirname, 'src')
    },
    extensions: ['.js', '.vue', '.json', '.css']
  },
  devServer: {
    host: '127.0.0.1',
    port: 8010,
    proxy: {
          '/login': {
            target: 'http://127.0.0.1:8080',
            changeOrigin: true,
            pathRewrite: {
              '^/login': '/login'
            }
          },
    	  "/sysmenu":{
    		  target: 'http://127.0.0.1:8080',
    		  changeOrigin: true,
    		  pathRewrite: {
    		    "^/sysmenu": "/sysmenu"
    		  }
    	  },
    	  "/logout":{
    		  target:'http://127.0.0.1:8080',
    		  changeOrigin:true,
    		  pathRewrite: {
    		    "^/logout": "/logout"
    		  }
    	  },
    	  '/user/basic':{
    		  target:'http://127.0.0.1:8080',
    		  changeOrigin:true,
    		  pathRewrite: {
    		    '^user/basic': '/user/basic'
    		  }
    	  },
    	  '/dept/basic':{
    	  		  target:'http://127.0.0.1:8080',
    	  		  changeOrigin:true,
    	  		  pathRewrite: {
    	  		    '^dept/basic': '/dept/basic'
    	  		  }
    	  },
    	  '/class/basic':{
    	  		  target:'http://127.0.0.1:8080',
    	  		  changeOrigin:true,
    	  		  pathRewrite: {
    	  		    '^class/basic': '/class/basic'
    	  		  }
    	  },
		  '/teacher/basic':{
		  		  target:'http://127.0.0.1:8080',
		  		  changeOrigin:true,
		  		  pathRewrite: {
		  		    '^teacher/basic': '/teacher/basic'
		  		  }
		  },
		  '/major/basic':{
		  		  target:'http://127.0.0.1:8080',
		  		  changeOrigin:true,
		  		  pathRewrite: {
		  		    '^major/basic': '/major/basic'
		  		  }
		  },
		  '/teachBuild/basic':{
		  		  target:'http://127.0.0.1:8080',
		  		  changeOrigin:true,
		  		  pathRewrite: {
		  		    '^teachBuild/basic': '/teachBuild/basic'
		  		  }
		  },
		  '/course/basic':{
		  		  target:'http://127.0.0.1:8080',
		  		  changeOrigin:true,
		  		  pathRewrite: {
		  		    '^course/basic': '/course/basic'
		  		  }
		  }
        },
    historyApiFallback: {
      index: url.parse(options.dev ? '/assets/' : publicPath).pathname
    }
  },
  devtool: options.dev ? '#eval-source-map' : '#source-map'
})
