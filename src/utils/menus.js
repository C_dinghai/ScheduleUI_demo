import {getRequest} from "./api";

export const initMenu = (router, store) => {
    if (store.state.routes.length > 0) {
        return;
    }
    getRequest("/sysmenu").then(data => {
        if (data) {
            let fmtRoutes = formatRoutes(data);
            router.addRoutes(fmtRoutes);
            store.commit('initRoutes', fmtRoutes);
        }
    })
}
export const formatRoutes = (routes) => {
    let fmRoutes = [];
    routes.forEach(router => {
        let {
            path,
            component,
            name,
            iconCls,
			meta,
            children
        } = router;
        if (children && children instanceof Array) {
            children = formatRoutes(children);
        }
        let fmRouter = {
            path: path,
            name: name,
            iconCls: iconCls,
			meta: meta,
            children: children,
            component(resolve) {
               if(component.startsWith("home")){
				   require(['../components/homepage.vue'],resolve)
			   }else if(component.startsWith("Cla")){
				   require(['../components/view/Class.vue'],resolve)
			   }else if(component.startsWith("Dep")){
				   require(['../components/view/SysDept.vue'],resolve)
			   }else if(component.startsWith("User")){
				   require(['../components/view/UserConfig.vue'],resolve)
			   }else if(component.startsWith("Cour")){
				   require(['../components/view/Course.vue'],resolve)
			   }else if(component.startsWith("Tea")){
				   require(['../components/view/Teacher.vue'],resolve)
			   }			   
            }
        }
        fmRoutes.push(fmRouter);
    })
    return fmRoutes;
}